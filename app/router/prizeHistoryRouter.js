// Khai báo thư viện express
const express = require('express');


//Import  Controller
const { createPrizeHistory, getAllPrizeHistory, getPrizeHistoryById, deletePrizeHistoryById, getUpdatePrizeHistory } =
    require('../controller/prizeHistoryController');

// Tạo router
const router = express.Router();

//CREATE A Dice History
router.post('/prize-histories', createPrizeHistory);
// get all prizes history
router.get('/prize-histories', getAllPrizeHistory);
// get by id
router.get('/prize-histories/:historyId', getPrizeHistoryById);
// get update Prize
router.put('/prize-histories/:historyId', getUpdatePrizeHistory);
// deletePrizeById
router.delete('/prize-histories/:historyId', deletePrizeHistoryById)
//export 

//export
module.exports = router;

